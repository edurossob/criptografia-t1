import random
import re
import string

'''
3. Determinar quantas colunas sao adequadas. num_colunas
    3.1 shifta len % 25 >> do numero da letra, no caso a=1, entao a=1
    3.2 soma os valores de cada letra da chave 
    3.3 resultado sera quantas colunas serao utilizdas. 
'''
def define_colunas(key):

    # Regex para remover todos os caracteres q nao sao letras de a-z
    regex = re.compile(r"[^\w]|[\d]")
    key = regex.sub("", key.lower())
    key = key.replace(" ", "").lower()

    # define o alfabeto
    alphabet = {'a':1,"b":2,"c":3,
                'd':4,"e":5,"f":6,
                'g':7,'h':8,'i':9,
                'j':10,'k':11,'l':12,
                'm':13,'n':14,'o':15,
                'p':16, 'q':17, 'r':18,
                's':19, 't':20, 'u':21,
                'v':22, 'w':23, 'x':24,
                'y':25, 'z':26
                }

    # Define o len do alfabeto
    amount_shift = len(key) % 25

    # Shifta o alfabeto
    for i in alphabet:
        alphabet[i] = (alphabet[i] + amount_shift) % 26
        if alphabet[i] == 0:
            alphabet[i] = 26

    # Soma as letras do alfabeto
    soma = 0
    for letra in key:
        soma+=alphabet[letra]

    # Se nenhuma letra for valida, entao a chave sera 1.
    if soma == 0:
        soma = 1
    return soma
    
def transpoe(texto, chave):
    tam_texto = len(texto)
    num_colunas = define_colunas(chave)
    num_linhas =  (tam_texto // num_colunas) +1
    
    grid = [[random.choice(string.ascii_letters).lower() for i in range(num_colunas)] for j in range(num_linhas)]
    
    indices = [i for i in range(num_colunas)]

    texto += '@'

    # Fill the grid with the plaintext
    for i, letter in enumerate(texto):
        row = i // num_colunas
        col = i % num_colunas
        grid[row][col] = letter

    random.Random(len(chave)).shuffle(indices)


    texto_transposto = ""
    for row in grid:
        for i in indices:
            texto_transposto += row[i]
    return texto_transposto


def destranspoe(texto, chave):
    tam_texto = len(texto)
    num_colunas = define_colunas(chave)
    num_linhas =  (tam_texto // num_colunas) +1
    
    grid = [[random.choice(string.ascii_letters).lower() for i in range(num_colunas)] for j in range(num_linhas)]
    
    indices = [i for i in range(num_colunas)]

    # Fill the grid with the plaintext
    for i, letter in enumerate(texto):
        row = i // num_colunas
        col = i % num_colunas
        grid[row][col] = letter

    indices_transpostos = indices
    random.Random(len(chave)).shuffle(indices_transpostos)


    indices_retorno = sorted(indices, key=lambda k: indices[k])

    texto_transposto = ""
    for row in grid:
        for i in indices_retorno:
            texto_transposto += row[i]
    return texto_transposto



# Procura a chave que contem o primeiro valor key no dicionario
def dict_key(dicionario,key):
    for i in dicionario:
        if dicionario[i] == key:
            return i
    return ' '

# Cifra um texto utilizando uma chave, com tecnica de substituicao
def crypt_subs(texto, chave):
    alphabet = {'a':1,"b":2,"c":3,
                'd':4,"e":5,"f":6,
                'g':7,'h':8,'i':9,
                'j':10,'k':11,'l':12,
                'm':13,'n':14,'o':15,
                'p':16, 'q':17, 'r':18,
                's':19, 't':20, 'u':21,
                'v':22, 'w':23, 'x':24,
                'y':25, 'z':26,' ':27, "@":28
                }
    tam_chave = len(chave)
    arr_text  = []
    index_chave = 0

    # Para cada letra do texto, soma com a correspondente da chave (em circular)
    for i in texto:
        if((alphabet[i]+alphabet[chave[index_chave]]) % 28) == 0:
            arr_text.append(dict_key(alphabet,28))
        else:
            arr_text.append(dict_key(alphabet,(alphabet[i]+alphabet[chave[index_chave]]) % 28))
        index_chave+=1
        if index_chave >= tam_chave:
            index_chave=0
    print(arr_text)
    return ''.join(arr_text)

# Decifra um texto utilizando uma chave, com tecnica de substituicao
def decrypt_subs(texto, chave):
    alphabet = {'a':1,"b":2,"c":3,
                'd':4,"e":5,"f":6,
                'g':7,'h':8,'i':9,
                'j':10,'k':11,'l':12,
                'm':13,'n':14,'o':15,
                'p':16, 'q':17, 'r':18,
                's':19, 't':20, 'u':21,
                'v':22, 'w':23, 'x':24,
                'y':25, 'z':26, ' ':27, "@":28
                }
    tam_chave = len(chave)
    arr_text  = []
    index_chave = 0
    for i in texto:
        if((alphabet[i]-alphabet[chave[index_chave]]) % 28) == 0:
            arr_text.append(dict_key(alphabet,28))
        else:
            arr_text.append(dict_key(alphabet,(alphabet[i]-alphabet[chave[index_chave]]) % 28))
        index_chave+=1
        if index_chave >= tam_chave:
            index_chave=0
    return ''.join(arr_text)

def limpa_textos(texto):
    regex = re.compile(r"[^\w]|[\d]|[\s]")
    texto = regex.sub("", texto.lower())
    return texto

def criptografa(texto, chave):

    texto_transposto = transpoe(texto, chave)
    texto_cripto = crypt_subs(texto_transposto, chave)

    print(f"Texto critpografado:\n{texto_cripto}")
    return texto_cripto

def descriptografa(texto, chave):
    texto_1 = decrypt_subs(texto, chave)
    texto_2 = destranspoe(texto_1, chave)
    texto_2 = texto_2.split("@")[0]
    print(f"Texto descriptografado:\n{texto_2}")

    return texto_2

texto_limpo = input("Digite o texto que deseja criptografar:\n")
chave = input("Digite a chave:\n")
cripto = criptografa(texto_limpo, chave)
descriptografa(cripto, chave)

