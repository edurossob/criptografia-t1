# Algoritmo da criptografia




1. Converter letra para binario de acordo com a tabela braile 
A -> (⠁) -> 10 -> 0b100000
            00
            00

B -> (⠃) -> 10 -> 0b101000
            10
            00 
...

2. Pegar a primeira letra e metade da segunda, gerando um valor de 9 bits e uma matriz 3x3
A = 0b100000 concatenado com a primeira metade de B = 0b101 teremos 0b100000101
Iremos transformar em =  [[100]
                          [000]
                          [101]]

A segunda metade de B será unida com a próxima letra na segunda iteração do algoritmo 
e passará pelo mesmo processo a seguir .


3. Pegar o timestamp atual invertido e converter para uma matriz 3x3

Ex Timestamp: 1671590731
= 1 3 7 
  0 9 5
  1 7 61

Verificar se é inversivel.
NAO inversivel -> somar 100 e tentar novamente.
INVERSIVEL -> continuar.


4. Somar a matriz obtida no passo 2 com a matriz obtida no passo 3.
ex:

1 0 0   1 3 7    1 3 7    
0 0 0 + 0 9 5  = 0 0 0     
1 0 1   1 7 61   2 10 68    

5. Unir os numeros da matriz resultante
13700021068

6. Converter para binario
1100110000100101011011101101001100

7. contar quantos caracteres deu
34.

8. Para reconstruir posteriormente a matriz, iremos guardar também 
    o numero de casas decimais para cada indice da matriz resultante

1 1 1 1 1 1 1 2 2
111111122

9. Converter para binario e concatenar com o outro binario 
111111122 = 110100111110110101111010010

=>

1100110000100101011011101101001100 110100111110110101111010010

=
1100110000100101011011101101001100110100111110110101111010010


10. Incluir o numero de caracteres como primeiro indice. Em 8 bits
34 = 00100010

00100010 1100110000100101011011101101001100110100111110110101111010010
001000101100110000100101011011101101001100110100111110110101111010010


Resumo até agora: 


001000101100110000100101011011101101001100110100111110110101111010010

Num de caracteres da matriz | caracteres | Casas decimais de cada indice
34 | 13700021068 | 111111122
8 bits | 34 bits | 27 bits
00100010 1100110000100101011011101101001100 110100111110110101111010010



11. Separar este binario em 8 bits (completando com 0's à esquerda na ultima linha)
00100010
11001100
00100101
01101110
11010011
00110100
11111011
01011110
10100000

12. Com 8 bits temos um numero de 0 a 255, mapearemos em 87 símbolos
    Entre 0 e 86 podemos mapear diretamente, após isso introduzimos 4 caracteres especiais
    para simbolozar em qual volta estamos, de 4 voltas máximas.

00100010 - 34 - F
11001100 - 204 = (204%87 = 30) = Terceira volta B - -B
00100101 - 37 - I
...

13. Cocantenar as letras da conversão e formar a frase final 

F-BI....

14. repetir isso para todas os "pares" de letras a serem criptografadas 

15. ao final, converter o timestamp para binario e utilizar o mesmo mapeamento do alfabeto

16. concatenar o timestamp convertido ao final de toda a frase criptografada.
 
17. concatenar o tamanho do timestamp na ultima posicao da frase criptograda.