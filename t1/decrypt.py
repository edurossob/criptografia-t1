import time
import numpy as np


braile = {'100000' : 'a', '101000' : 'b', '110000' : 'c',
'110100' : 'd', '100100' : 'e', '111000' : 'f',
'111100' : 'g', '101100' : 'h', '011000' : 'i',
'011100' : 'j', '100010' : 'k', '101010' : 'l',
'110010' : 'm', '110110' : 'n', '100110' : 'o',
'111010' : 'p', '111110' : 'q', '101110' : 'r',
'011010' : 's', '011110' : 't', '100011' : 'u',
'101011' : 'v', '011101' : 'w', '110011' : 'x',
'110111' : 'y', '100111' : 'z', '000000' : ' '}

list_ascii = {'"' : '00000000', '#' : '00000001', '$' : '00000010',
'%' : '00000011', '&' : '00000100', "'" : '00000101',
'(' : '00000110', ')' : '00000111', '*' : '00001000', 
'+' : '00001001', ',' : '00001010', '.' : '00001011',
'/' : '00001100', '0' : '00001101', '1' : '00001110',
'2' : '00001111', '3' : '00010000', '4' : '00010001',
'5' : '00010010', '6' : '00010011', '7' : '00010100',
'8' : '00010101', '9' : '00010110', ':' : '00010111',
';' : '00011000', '<' : '00011001', '=' : '00011010',
'>' : '00011011', '?' : '00011100', 'A' : '00011101',
'B' : '00011110', 'C' : '00011111', 'D' : '00100000',
'E' : '00100001', 'F' : '00100010', 'G' : '00100011',
'H' : '00100100', 'I' : '00100101', 'J' : '00100110',
'K' : '00100111', 'L' : '00101000', 'M' : '00101001',
'N' : '00101010', 'O' : '00101011', 'P' : '00101100',
'Q' : '00101101', 'R' : '00101110', 'S' : '00101111',
'T' : '00110000', 'U' : '00110001', 'V' : '00110010',
'W' : '00110011', 'X' : '00110100', 'Y' : '00110101',
'Z' : '00110110', '[' : '00110111', ']' : '00111000',
'^' : '00111001', '_' : '00111010', '`' : '00111011',
'a' : '00111100', 'b' : '00111101', 'c' : '00111110',
'd' : '00111111', 'e' : '01000000', 'f' : '01000001',
'g' : '01000010', 'h' : '01000011', 'i' : '01000100',
'j' : '01000101', 'k' : '01000110', 'l' : '01000111',
'm' : '01001000', 'n' : '01001001', 'o' : '01001010',
'p' : '01001011', 'q' : '01001100', 'r' : '01001101',
's' : '01001110', 't' : '01001111', 'u' : '01010000',
'v' : '01010001', 'w' : '01010010', 'x' : '01010011',
'y' : '01010100', 'z' : '01010101', '{' : '01010110',
'}' : '01010111'}

list_round = ['~', '@', '-', '|']


def descriptografar(texto_criptografado):

    # passo um eh reverter o texto para a tabela ascii imaginaria e recrciar o array de binario
    arr_bin_chars = []
    index = 0
    while True:
        # texto possui voltas
        if texto_criptografado[index] in list_round:
            simbolo_atual = texto_criptografado[index]
            voltas = list_round.index(simbolo_atual)+1
            somar = 87*voltas
            char_atual = texto_criptografado[index+1]
            int_atual = int(list_ascii[char_atual],2) + somar
            binario_atual = str(bin(int_atual))[2:].zfill(8)
            arr_bin_chars.append(binario_atual)
            index+=2 # prox item
            
        else:
            binario_atual = texto_criptografado[index]
            index+=1
            arr_bin_chars.append(list_ascii[binario_atual])
        if index >= len(texto_criptografado):
            break

    tam_timestamp = int(arr_bin_chars[ len(arr_bin_chars) - 1 ],2)
    mod_ts = tam_timestamp % 8 # qnts bits sobram
    posicoes_timestamp = int((tam_timestamp - mod_ts) / 8)
    arr_bin_chars.pop()
    binario_timestamp = ""

    if(mod_ts > 0):
        temp = arr_bin_chars.pop()
        binario_timestamp+=temp[len(temp) - mod_ts : ]

    for i in range(1,posicoes_timestamp+1):
        binario_timestamp=arr_bin_chars.pop() + binario_timestamp

    timestamp_integer = int(binario_timestamp,2)
    timestamp_atual = str(timestamp_integer)
    tamanho_timestamp = len(timestamp_atual)

    matriz_timestamp = [[int(item) for item in (','.join(timestamp_atual[0:3])).split(',')],
                            [int(item) for item in (','.join(timestamp_atual[3:6])).split(',')],
                            [int(item) for item in (','.join(timestamp_atual[6:9])).split(',')]
                            ]


    np_ts_matrix = np.array(matriz_timestamp)

    tam_total = len(arr_bin_chars)
    arr_resolucao = []
    while True:

        tamanho_pt_1 = arr_bin_chars.pop(0)
        tamanho_pt_2 = arr_bin_chars.pop(0)
        
        tam_pt_1_int = int(tamanho_pt_1,2)
        tam_pt_2_int = int(tamanho_pt_2,2)
        

        index = 0
        texto_partes = ""
        tam_pt1_pt2 = tam_pt_1_int+tam_pt_2_int
        while index < (tam_pt1_pt2):
            texto_partes +=arr_bin_chars.pop(0)
            index+=8


        if(tam_pt1_pt2 < len(texto_partes)):
            restante = len(texto_partes) - tam_pt1_pt2
            ultima_parte = texto_partes[-8:]
            texto_partes = texto_partes[:-8]
            ultima_parte = ultima_parte[restante:]
            texto_partes+=ultima_parte

        
        parte1_str = str(int(texto_partes[:tam_pt_1_int],2))
        parte2_str = str(int(texto_partes[tam_pt_1_int:],2))

        arr_final = []
        arr_final = [
            [],[],[]    
        ]

        index = 0
        for i in range(0,9):
            arr_final[int(i/3)].append( int(parte1_str[index:index+int(parte2_str[i])]))
            index+=int(parte2_str[i])

        np_arr_atual = np.array(arr_final)

        matriz_sub = np_arr_atual -np_ts_matrix
        arr_resolucao.append(matriz_sub.tolist())
        

        if len(arr_bin_chars) < 1:
            break

    texto_limpo = ''
    for i in arr_resolucao:
        binario_formado = ''.join([str(j) for j in i[0]])+''.join([str(j) for j in i[1]])
        texto_limpo+=braile[binario_formado]




    # texto_limpo = texto_criptografado

    return texto_limpo

text = input("Digite o texto a ser descriptografado\n")
print(f"\nTexto descriptografado: \n{descriptografar(text)}")
