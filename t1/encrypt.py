import time
import numpy as np
def braile(x):
    match x:
        case 'a': return 0b100000
        case 'b': return 0b101000
        case 'c': return 0b110000
        case 'd': return 0b110100
        case 'e': return 0b100100
        case 'f': return 0b111000
        case 'g': return 0b111100
        case 'h': return 0b101100
        case 'i': return 0b011000
        case 'j': return 0b011100
        case 'k': return 0b100010
        case 'l': return 0b101010
        case 'm': return 0b110010
        case 'n': return 0b110110
        case 'o': return 0b100110
        case 'p': return 0b111010
        case 'q': return 0b111110
        case 'r': return 0b101110
        case 's': return 0b011010
        case 't': return 0b011110
        case 'u': return 0b100011
        case 'v': return 0b101011
        case 'w': return 0b011101
        case 'x': return 0b110011
        case 'y': return 0b110111
        case 'z': return 0b100111
        case ' ': return 0b000000

list_ascii = {
'00000000' : '"', '00000001' : '#', '00000010' : '$',
'00000011' : '%', '00000100' : '&', '00000101' : "'",
'00000110' : '(', '00000111' : ')', '00001000' : '*',
'00001001' : '+', '00001010' : ',', '00001011' : '.',
'00001100' : '/', '00001101' : '0', '00001110' : '1',
'00001111' : '2', '00010000' : '3', '00010001' : '4',
'00010010' : '5', '00010011' : '6', '00010100' : '7',
'00010101' : '8', '00010110' : '9', '00010111' : ':',
'00011000' : ';', '00011001' : '<', '00011010' : '=',
'00011011' : '>', '00011100' : '?', '00011101' : 'A',
'00011110' : 'B', '00011111' : 'C', '00100000' : 'D',
'00100001' : 'E', '00100010' : 'F', '00100011' : 'G',
'00100100' : 'H', '00100101' : 'I', '00100110' : 'J',
'00100111' : 'K', '00101000' : 'L', '00101001' : 'M',
'00101010' : 'N', '00101011' : 'O', '00101100' : 'P',
'00101101' : 'Q', '00101110' : 'R', '00101111' : 'S',
'00110000' : 'T', '00110001' : 'U', '00110010' : 'V',
'00110011' : 'W', '00110100' : 'X', '00110101' : 'Y',
'00110110' : 'Z', '00110111' : '[', '00111000' : ']',
'00111001' : '^', '00111010' : '_', '00111011' : '`',
'00111100' : 'a', '00111101' : 'b', '00111110' : 'c',
'00111111' : 'd', '01000000' : 'e', '01000001' : 'f',
'01000010' : 'g', '01000011' : 'h', '01000100' : 'i',
'01000101' : 'j', '01000110' : 'k', '01000111' : 'l',
'01001000' : 'm', '01001001' : 'n', '01001010' : 'o',
'01001011' : 'p', '01001100' : 'q', '01001101' : 'r',
'01001110' : 's', '01001111' : 't', '01010000' : 'u',
'01010001' : 'v', '01010010' : 'w', '01010011' : 'x',
'01010100' : 'y', '01010101' : 'z', '01010110' : '{',
'01010111' : '}'}

list_round = ['~', '@', '-', '|']

def criptografar(texto):
    
    # converte o texto para o alfabeto em braile
    arr_binario = []

    index = 0
    for i in texto:
        arr_binario.insert(index, f'{braile(i):06b}')
        index+=1

    # Criar a matriz binaria
    arr_matriz = []
    for i in range(0,index):

        string = arr_binario[i]
        
        # Caso exista a prox palavra
        if(i < (index-1)):
            prox_string = arr_binario[i+1]
        else:
            prox_string =  f'{braile("i"):06b}'
        #forma a mtriz
        matriz_atual = [[int(item) for item in (','.join(string[0:3])).split(',')],
                        [int(item) for item in (','.join(string[3:6])).split(',')], 
                        [int(item) for item in (','.join(prox_string[0:3])).split(',')]]

        arr_matriz.append(matriz_atual)

    ts_atual = int(time.time())
    # Encontra matriz do timestamp com det != 0
    while True:
        timestamp_atual = str(ts_atual)[::-1]

        tamanho_timestamp = len(timestamp_atual)

        matriz_timestamp = [[int(item) for item in (','.join(timestamp_atual[0:3])).split(',')],
                            [int(item) for item in (','.join(timestamp_atual[3:6])).split(',')],
                            [int(item) for item in (','.join(timestamp_atual[6:9])).split(',')]
                            ]


        np_ts_matrix = np.array(matriz_timestamp)
        det = np.linalg.det(np_ts_matrix)
        if(det != 0):
            break 
        else:
            ts_atual+=100

    arr_bin_chars = []

    for i in arr_matriz:
        np_atual = np.array(i)
        np_matrix_multiplicada = np_atual + np_ts_matrix
        np_matrix_str = ""
        matrix_decimal_len = ""

        for i in np_matrix_multiplicada:
            for j in i:
                np_matrix_str+=str(j)
                matrix_decimal_len+=str(len(str(j)))

        np_matrix_str_len_first = len(np_matrix_str)
        np_matrix_str_len = len(str(int(np_matrix_str)))

        binario_final = bin(int(np_matrix_str))
        binario_len = bin(int(matrix_decimal_len))
        tamanho_binario = len(str(binario_final)[2:])

        binarios_concat = str(binario_final[2:])+ str(binario_len[2:])


        bits_faltantes = len(binarios_concat) % 8
        tamanho_binario_len = len(str(binario_len[2:]))
        arr_bin_chars.append(str(bin(tamanho_binario))[2:].zfill(8))
        arr_bin_chars.append(str(bin(tamanho_binario_len))[2:].zfill(8))
        
        for i in range(0, len(binarios_concat), 8):
            arr_bin_chars.append(binarios_concat[i:i+8].zfill(8))

        
 
    texto_cifrado = ""

    # parte do timestamp
    timestamp_as_str = str(bin(int(timestamp_atual)))[2:]
    binario_timestamp = str(bin(len(timestamp_as_str)))[2:].zfill(8)
    
    for i in range(0, len(timestamp_as_str), 8):
        arr_bin_chars.append(timestamp_as_str[i:i+8].zfill(8))

 
    for i in arr_bin_chars:
        parte_inserir = ""
        try:
            parte_inserir+=list_ascii[i]
        except:
            voltas = int(int(i,2)/87)
            i_mod = int(i,2) - (voltas * 87)
            parte_inserir+=f"{list_round[voltas-1]}{list_ascii[str(bin(i_mod))[2:].zfill(8)]}"
        texto_cifrado+=parte_inserir
    texto_cifrado+=list_ascii[binario_timestamp]

    return texto_cifrado


texto_limpo = input("Digite o texto que deseja criptografar:\n")

print(f"\nTexto criptografado: \n{criptografar(texto_limpo)}")
